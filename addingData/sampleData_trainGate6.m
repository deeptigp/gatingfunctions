clear;
load('Data/goldLayerLabelData_500C.mat');
load('Data/TrainingNIter_500Class.mat');
load('Data/TestingNIter_500Class.mat');

gTrainIndx = TrainingNIter(:,1);
gTestIndx = TestingNIter(:,1);

% Sample positive negative samples from the training data.
Y = goldLayerLabelData.layerLabels(gTrainIndx);

% We want to only get that many positive samples.
negIndx  = find(Y~=2); % Doing this will include "dont care labels" in the negative samples
%negIndx  = find(Y == 3 | Y ==1) ; % This excludes the "dont care labels" from the negative samples.
posIndx = find(Y == 2);


numNegSamples = length(negIndx);
numPosSamples = length(posIndx);



% Fewer positive than negative samples
if(numPosSamples < numNegSamples)
    randIndx = randperm(length(negIndx),numPosSamples);
    negIndx = negIndx(randIndx);
elseif(numNegSamples < numPosSamples)
    randIndx = randperm(length(posIndx),numNegSamples);
    posIndx = posIndx(randIndx);
end

% Sampled labels
posSamples = Y(posIndx);
negSamples = Y(negIndx);

% indices relative to length of gtrainIndx
dataIndices = [posIndx' negIndx'];

classLabels = double([posSamples' negSamples']);

classLabels = double(classLabels == 2);


numImages = length(dataIndices);

indx = randperm(numImages);

% Just randomizing the indices.
dataIndices = dataIndices(indx);
classLabels = classLabels(indx);

trainIndx = gTrainIndx(dataIndices);


%
sampledData = struct;

sampledData.trainLabels = classLabels;
sampledData.trainIndx = trainIndx;

save('Data/sampledData_500C_gate6_noExclude.mat','sampledData');
