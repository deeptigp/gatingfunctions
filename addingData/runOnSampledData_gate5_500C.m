load('Data/sampledData_500C_gate5.mat');
load('Data/goldLayerLabelData_500C.mat');
load('Data/TestingNIter_500Class.mat');
featType = 'pool5';


addpath('../include/');
addpath('../debug_gating/');

DATA_PATH = '/media/deepti/J/MSR/imNetVal/Data/'; % fix the data path accordingly.

gTestIndx = TestingNIter(:,1);

if(strcmp(featType,'pool5'))
    load(strcat(DATA_PATH,'imNetVal_pool5Features_1_500.mat'));
    features = pool5Features;
elseif(strcmp(featType,'fc6'))
    load(strcat(DATA_PATH,'imNetVal_fc6Features_1_500.mat'));
    features = fc6Features;
elseif(strcmp(featType,'fc7'))
    load(strcat(DATA_PATH,'imNetVal_fc7Features_1_500.mat'));
    features = fc7Features;
end

trainLabels = sampledData.trainLabels';
testLabels = goldLayerLabelData.layerLabels(gTestIndx);

testLabels = double(testLabels == 1)';

numTrees = 1200;

trainNX = features(sampledData.trainIndx,:);

testNX = features(gTestIndx,:);

disp('The sizes of the train and test vectors are')
[size(trainNX) size(trainLabels)]
[size(testNX) size(testLabels)]

[predictedLabels, runTimeForAllImgs, scores, stdevs, BaggedEnsemble] = ensembleDecisionForests(trainNX, trainLabels, testNX, testLabels, numTrees);

[size(predictedLabels) size(testLabels)]

[accurEx,precEx,recEx,accurIn0,precIn0,recIn0,accurIn1,precIn1,recIn1] = evaluateClassifier_gate5(predictedLabels, 'Data/goldLayerLabelData_500C.mat',gTestIndx);

gatingFunc5_500C_1 = struct;
gatingFunc5_500C_1.trainIndx = sampledData.trainIndx;
gatingFunc5_500C_1.testIndx = gTestIndx;
gatingFunc5_500C_1.predictedLabels = predictedLabels;
gatingFunc5_500C_1.testLabels = testLabels;
save('Data/gatingFunc5_500C_1.mat','gatingFunc5_500C_1');
