%[accurEx,precEx,recEx,accurIn,precIn,recIn] = evaluateClassifier(predictedLabels, goldLabelsFile, testFile, trainTestSplit)
function [accurEx,precEx,recEx,accurIn0,precIn0,recIn0,accurIn1,precIn1,recIn1] = evaluateClassifier_gate5(predictedLabels, goldLabelsFile, testFile, trainTestSplit)
    addpath('../include');
    trainTestSplit = 1;
     load(goldLabelsFile);
      
     load(testFile);
    
    testIndx = TestingNIter(:,trainTestSplit);
    
    testLabels = goldLayerLabelData.layerLabels(testIndx);
    
    dontCareIndx = find(testLabels ==1 | testLabels ==0);
    
    % Consider only those indices which are either 1 or 2/3
    excludeDontCareIndx = (testLabels ~=0);
    
    gT1 = testLabels(excludeDontCareIndx);
    gT1 = double(gT1 ==1);
    
    gT2 = double(testLabels == 1);
    
    predY1  = predictedLabels(excludeDontCareIndx);
    
    testLabels(dontCareIndx) = 1;
    gT3 = double(testLabels == 1);
    
%     [size(gT1) size(predY1)]
%     
%     [size(gT2) size(predictedLabels)]
%     
%     [size(gT3) size(predictedLabels)]
%    
    [accurEx,precEx,recEx] = computeAccurPrecRecall_binary(gT1,predY1);
   
    [accurIn0,precIn0,recIn0] = computeAccurPrecRecall_binary(gT2,predictedLabels);
    
    [accurIn1,precIn1,recIn1] = computeAccurPrecRecall_binary(gT3,predictedLabels);
end