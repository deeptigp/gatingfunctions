function [predictedLabels, runTimeForAllImgs, scores, stdevs, BaggedEnsemble] = ensembleDecisionForests(trainX, trainY, testX, testY, numTrees)
    rng(1945,'twister');
    %numTrees = 1000;

   addpath('../include/');
   
    BaggedEnsemble = TreeBagger(numTrees,trainX,trainY);%,'OOBPrediction','on');%,'InBagFraction',0.3,'SampleWithReplacement','on')
    
%     BaggedEnsemble.DefaultYfit = '';
%     
%     oobErrorBaggedEnsemble = oobError(BaggedEnsemble);
%     plot(oobErrorBaggedEnsemble)
%     xlabel 'Number of grown trees';
%     ylabel 'Out-of-bag classification error';

    
    tStart = tic; 
    
    [Y_hat,scores,stdevs] = predict(BaggedEnsemble,testX);
    
    runTimeForAllImgs = toc(tStart);
    
    predictedLabels = zeros(length(testY),1);
    
    for i = 1:length(testY)
        predictedLabels(i) = str2num(Y_hat{i,1});
    end
end