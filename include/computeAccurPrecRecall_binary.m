function [accur, prec, recall] = computeAccurPrecRecall_binary(gLabels, predY)
    cMat = confusionmat(gLabels, predY);
    
    %% wrt class 1
    recall = cMat(2,2)/(cMat(2,2)+cMat(2,1));
    
    prec = cMat(2,2)/(cMat(2,2)+cMat(1,2));
    
    accur = mean(gLabels == predY);
end