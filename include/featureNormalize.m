function [trainNX, testNX] =featureNormalize(trainNX, testNX)
    meanFeat = mean(trainNX,1);
    rangeFeat = std(trainNX,1);

    trainNX = (trainNX - repmat(meanFeat, size(trainNX, 1), 1)) ./ repmat(rangeFeat, size(trainNX, 1), 1);
    testNX = (testNX - repmat(meanFeat, size(testNX, 1), 1)) ./ repmat(rangeFeat, size(testNX, 1), 1);
end