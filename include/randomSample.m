function feat = randomSample(feat, K)
    % K is the dimension to which we want to reduce the feat dim.
    featDim = size(feat,2);
    indx =  randperm(featDim, K);
    feat = feat(:,indx);
end